# -*- coding: utf8 -*-
from selenium.webdriver.common.action_chains import ActionChains


class PageMenu(object):
    _locators = {
        'source': "//div[@class='ui-widget-content ui-draggable ui-draggable-handle']",
        'target': "//div[@class='ui-widget-header ui-droppable']",
        'frame': "//*[@id='example-1-tab-1']//iframe[@class='demo-frame']",
    }

    def drag_and_drop(self, driver):
        source = driver.find_element_by_xpath(self._locators['source'])
        target = driver.find_element_by_xpath(self._locators['target'])
        action_chains = ActionChains(driver)
        action_chains.drag_and_drop(source, target).perform()
        return target

    def set_frame(self, driver):
        frame = driver.find_element_by_xpath(self._locators['frame'])
        driver.switch_to_frame(frame)
