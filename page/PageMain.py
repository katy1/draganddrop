# -*- coding: utf8 -*-
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class PageMain(object):
    _locators = {
        'sign_in': "//a[text()='Signin']",
        'login': "//*[@id='login']//fieldset[1]/input",
        'password': "//*[@id='login']//fieldset[2]/input",
        'button': ".//*[@id='login']//div[2]/input",
        'interaction': "//*[@id='toggleNav']/li[2]/a",
        'drop': "//*[@id='toggleNav']/li[2]/ul/li[2]/a",
    }

    def __find_button(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def __find_element(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def click_button_signin(self, driver):
        self.__find_button(
            driver, self._locators['sign_in']).click()

    def click_button_auth(self, driver):
        self.__find_button(
            driver, self._locators['button']).click()

    def set_value_login(self, driver, value):
        self.__find_element(
            driver, self._locators['login']).send_keys(value)

    def set_value_password(self, driver, value):
        self.__find_element(
            driver, self._locators['password']).send_keys(value)

    def click_menu_interaction(self, driver):
        self.__find_button(
            driver, self._locators['interaction']).click()

    def click_drop(self, driver):
        self.__find_button(
            driver, self._locators['drop']).click()
