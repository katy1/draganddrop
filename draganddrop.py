from selenium import webdriver
import unittest
from page.PageMenu import PageMenu
from MethodMain import MethodMain
from page.PageMain import PageMain


class TestCase(unittest.TestCase):
    def setUp(self):
        self.page_main = PageMain()
        self.page_menu = PageMenu()
        self.page_method_main = MethodMain()
        self.driver = webdriver.Firefox()

    def test_drop(self):
        driver = self.driver
        driver.get("http://way2automation.com/way2auto_jquery/menu.php")
        driver.switch_to_window(driver.window_handles[-1])
        self.page_method_main.authorization(self.driver)
        self.driver.refresh()
        self.page_main.click_menu_interaction(driver)
        self.page_main.click_drop(driver)
        self.page_menu.set_frame(driver)
        target = self.page_menu.drag_and_drop(driver)
        self.assertEquals("Dropped!", target.text)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
